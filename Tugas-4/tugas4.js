//Soal 1
var i = 2;
console.log("LOOPING PERTAMA");
while (i<=20){
	console.log(i+" - I love coding");
	i+=2;
}
i-=2;
console.log("LOOPING KEDUA");
while(i>=2){
	console.log(i+" - I will become a frontend developer");
	i-=2;
}

//Soal 2
console.log("Soal 2");
for (var j=1;j<=20;j++){
	if(j%2==1 && j%3==0){
		console.log(j+" - I Love Coding");
	}
	else if(j%2==0){
		console.log(j+" - Berkualitas");	
	}
	else{
		console.log(j+" - Santai");
	}
}

//Soal 3
for (var k=1;k<=7;k++){
	console.log("#".repeat(k));	
}

//Soal 4
var kalimat="saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah = (daftarBuah.sort());
for(var C=0;C<daftarBuah.length;C++){
	console.log(daftarBuah[C]);
}
